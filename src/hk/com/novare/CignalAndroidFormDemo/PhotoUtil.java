/**
 * (c) Copyright 2010 Novare Technologies, Inc.
 * All rights reserved. All other trademarks and copyrights referred to herein
 * are the property of their respective holders. No part of this code may be
 * reproduced in any form or by any means or used to take any derivative work,
 * without written permission from Novare Technologies, Inc.
 * 
 * @file PhotoUtil.java
 * @brief Contains the PhotoUtil class, the utility class of the Phonebook application for retrieving, decoding and loading photos.
 * @author Wyndale Wong
 */
package hk.com.novare.CignalAndroidFormDemo;

import android.content.Context;
import android.graphics.*;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * A utility class that abstracts methods for retrieving, decoding and loading
 * photos.
 * 
 * @author Wyndale Wong
 */
public class PhotoUtil {

	/** Defined base value for sample size. */
	public static final int SAMPLE_SIZE_BASE = 2;

	/** Defined max image size in bytes for large photo decoding. */
	public static final int MAX_SIZE_IN_BYTES_HIGH_RES = 500000;

	/** Defined max image size in bytes for small photo decoding. */
	public static final int MAX_SIZE_IN_BYTES_LOW_RES = 100000;

	/** The defined string for this utility class's log tag. */
	private static final String TAG = PhotoUtil.class.getSimpleName();

	/**
	 * Private constructor for this class.
	 */
	private PhotoUtil() {
		// constructor, do nothing
	}
	
	/**
	 * Decodes the byte array string and loads it to the image container.
	 * 
	 * @param context
	 *            The application context
	 * @param data
	 *            Byte array string of the photo
	 * @param photoIV
	 *            The image container
	 * @param isThumb
	 *            Whether the decoded photo is for thumbnail
	 * @param isHighRes
	 *            Whether the image is to be decoded with high resolution limit
	 * @return The decoded Bitmap photo
	 * @see #loadPhotoFromByteArrayString(android.content.Context, Object, boolean)
	 * @see #getRoundedCornerBitmap(android.graphics.Bitmap)
	 */
	public static Bitmap loadPhotoFromByteArrayString(final Context context, final Object data,
			final ImageView photoIV, final boolean isThumb, final boolean isHighRes) {
		Log.d(TAG, "invoked loadPhotoFromByteArrayString");

		final Bitmap photo = loadPhotoFromByteArrayString(context, data, isHighRes);

		photoIV.setScaleType(ImageView.ScaleType.FIT_XY);

		if (photo != null && photoIV != null) {
			photoIV.setImageBitmap(photo);
		} else if (isThumb) {
			photoIV.setImageResource(R.drawable.profile_photo_small);
		} else {
			photoIV.setImageResource(R.drawable.profile_photo_large);
		}

		return photo;
	}

	/**
	 * Decodes the photo from byte array string.
	 * 
	 * @param context
	 *            The application context
	 * @param data
	 *            The input data
	 * @param isHighRes
	 *            Whether the image is to be decoded with high resolution limit
	 * @return The decoded Bitmap photo
	 */
	public static Bitmap loadPhotoFromByteArrayString(final Context context, final Object data,
			final boolean isHighRes) {
		Log.d(TAG, "invoked loadPhotoFromByteArrayString");

		Bitmap photo = null;
		byte[] decodedImage = null;

		if (data instanceof String) {
			if (!"".equals(((String) data).trim()) && !((String) data).startsWith("<")) {
				decodedImage = ((String) data).getBytes();
			} 
		} else if (data instanceof byte[]) {
			Log.d(TAG, "data type of byte[]");
			decodedImage = (byte[]) data;
		} else {
			Log.e(TAG, "at loadPhotoFromByteArrayString, Invalid type of data");
			return null;
		}

		if (decodedImage != null) {
			int sampleSizeExponent = 0;
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inTempStorage = new byte[16 * 1024];
			options.inJustDecodeBounds = true;
			options.inSampleSize = (int) Math.pow(SAMPLE_SIZE_BASE, sampleSizeExponent);
			BitmapFactory.decodeByteArray(decodedImage, 0, decodedImage.length, options);
			Log.d(TAG, "at loadPhotoFromByteArrayString, options.outHeight = " + options.outHeight
					+ ", options.outWidth = " + options.outWidth);
			while (options.outHeight * options.outWidth * 2 > (isHighRes ? MAX_SIZE_IN_BYTES_HIGH_RES
					: MAX_SIZE_IN_BYTES_LOW_RES)) {
				sampleSizeExponent++;
				options.inSampleSize = (int) Math.pow(SAMPLE_SIZE_BASE, sampleSizeExponent);
				BitmapFactory.decodeByteArray(decodedImage, 0, decodedImage.length, options);
				Log.d(TAG, "at loadPhotoFromByteArrayString, options.outHeight = "
						+ options.outHeight + ", options.outWidth = " + options.outWidth);
			}
			Log.d(TAG,
					"at loadPhotoFromByteArrayString, new sample size index = "
							+ (int) Math.pow(SAMPLE_SIZE_BASE, sampleSizeExponent));
			options.inJustDecodeBounds = false;
			options.inSampleSize = (int) Math.pow(SAMPLE_SIZE_BASE, sampleSizeExponent);
			try {
				photo = BitmapFactory
						.decodeByteArray(decodedImage, 0, decodedImage.length, options);
			} catch (final OutOfMemoryError e) {
				Log.d(TAG, "OutOfMemoryError: " + e.getMessage(), e);
				photo = null;
			}
		}

		return photo;
	}

	/**
	 * Loads the photo from a file to the button container.
	 * 
	 * @param context
	 *            The application context
	 * @param filePath
	 *            The path of the file containing image data
	 * @param changePhotoButton
	 *            The image button container
	 * @param isHighRes
	 *            Whether the image is to be decoded with high resolution limit
	 * @return The decoded Bitmap photo
	 */
	public static Bitmap loadPhotoToButton(final Context context, final String filePath,
			final ImageButton changePhotoButton, final boolean isHighRes) {
		Log.d(TAG, "invoked loadPhotoToButton");
		Bitmap photo = null;
		if (filePath != null && (new File(filePath)).length() > 0) {
			int sampleSizeExponent = 0;
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inTempStorage = new byte[16 * 1024];
			options.inJustDecodeBounds = true;
			options.inSampleSize = (int) Math.pow(SAMPLE_SIZE_BASE, sampleSizeExponent);
			BitmapFactory.decodeFile(filePath, options);
			Log.d(TAG, "at loadPhotoToButton, options.outHeight = " + options.outHeight
					+ ", options.outWidth = " + options.outWidth);
			while (options.outHeight * options.outWidth * 2 > (isHighRes ? MAX_SIZE_IN_BYTES_HIGH_RES
					: MAX_SIZE_IN_BYTES_LOW_RES)) {
				sampleSizeExponent++;
				options.inSampleSize = (int) Math.pow(SAMPLE_SIZE_BASE, sampleSizeExponent);
				BitmapFactory.decodeFile(filePath, options);
				Log.d(TAG, "at loadPhotoToButton, options.outHeight = " + options.outHeight
						+ ", options.outWidth = " + options.outWidth);
			}
			Log.d(TAG,
					"at loadPhotoToButton, new sample size index = "
							+ (int) Math.pow(SAMPLE_SIZE_BASE, sampleSizeExponent));
			options.inJustDecodeBounds = false;
			options.inSampleSize = (int) Math.pow(SAMPLE_SIZE_BASE, sampleSizeExponent);

			try {
				photo = BitmapFactory.decodeFile(filePath, options);
			} catch (final OutOfMemoryError e) {
				Log.d(TAG, "OutOfMemoryError: " + e.getMessage(), e);
				photo = null;
			}

			if (changePhotoButton != null) {
				if (photo != null) {
					changePhotoButton.setImageBitmap(photo);
				} else {
					changePhotoButton.setImageResource(R.drawable.profile_photo_large);
				}
				changePhotoButton.setScaleType(ImageView.ScaleType.FIT_XY);
			}
		} else {
			if (changePhotoButton != null) {
				changePhotoButton.setImageResource(R.drawable.profile_photo_large);
				changePhotoButton.setScaleType(ImageView.ScaleType.FIT_XY);
			}
		}
		return photo;
	}

	/**
	 * Retrieves the photo from local directory.
	 * 
	 * @param file
	 *            The stored photo location
	 * @param isHighRes
	 *            Whether the image is to be decoded with high resolution limit
	 * @return The decoded Bitmap photo
	 */
	public static Bitmap getPhotoFromFile(final File file, final boolean isHighRes) {
		Bitmap photo = null;
		int sampleSizeExponent = 0;

		if (file != null) {
			try {
				final BitmapFactory.Options options = new BitmapFactory.Options();
				options.inTempStorage = new byte[16 * 1024];
				options.inJustDecodeBounds = true;
				options.inSampleSize = (int) Math.pow(SAMPLE_SIZE_BASE, sampleSizeExponent);
				FileInputStream fis = new FileInputStream(file);
				BitmapFactory.decodeStream(fis, null, options);
				while (options.outHeight * options.outWidth * 2 > (isHighRes ? MAX_SIZE_IN_BYTES_HIGH_RES
						: MAX_SIZE_IN_BYTES_LOW_RES)) {
					sampleSizeExponent++;
					options.inSampleSize = (int) Math.pow(SAMPLE_SIZE_BASE, sampleSizeExponent);
					fis.close();
					fis = new FileInputStream(file);
					BitmapFactory.decodeStream(fis, null, options);
				}
				fis.close();
				options.inJustDecodeBounds = false;
				options.inSampleSize = (int) Math.pow(SAMPLE_SIZE_BASE, sampleSizeExponent);
				fis = new FileInputStream(file);
				photo = BitmapFactory.decodeStream(new FileInputStream(file), null, options);
				fis.close();
				
			} catch (final OutOfMemoryError e) {
				Log.d(TAG, "OutOfMemoryError: " + e.getMessage(), e);
				photo = null;
			} catch (final FileNotFoundException e) {
				Log.d(TAG, "FileNotFoundException: " + e.getMessage(), e);
			} catch (final IOException e) {
				Log.d(TAG, "IOException: " + e.getMessage(), e);
			}
		}
		return photo;
	}
	
	/**
	 * Convenience method that implements rounded corners for Bitmap images.
	 * 
	 * @param bitmap
	 *            The input Bitmap image
	 * @return The processed Bitmap photo
	 */
	public static Bitmap getRoundedCornerBitmap(final Bitmap bitmap) {
		Log.d(TAG, "invoked getRoundedCornerBitmap");
		final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(),
				Config.ARGB_8888);
		final Canvas canvas = new Canvas(output);
		final int color = 0xffFFFFFF;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		int largerDimensionLength = 0;
		if (bitmap.getWidth() > bitmap.getHeight()) {
			Log.d(TAG, "at getRoundedCornerBitmap, width is larger");
			largerDimensionLength = bitmap.getWidth();
		} else {
			Log.d(TAG, "at getRoundedCornerBitmap, height is larger");
			largerDimensionLength = bitmap.getHeight();
		}
		final float roundPx = (float) Math.pow(largerDimensionLength, 0.5);
		Log.d(TAG, "at getRoundedCornerBitmap, roundPx = " + roundPx);
		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);
		return output;
	}

}
