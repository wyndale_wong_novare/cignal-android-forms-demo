package hk.com.novare.CignalAndroidFormDemo;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.*;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.*;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;


public class SignatureActivity extends Activity implements View.OnClickListener {

    private final String TAG = SignatureActivity.class.getName();

    private Button useSignatureBtn;
    private Button clearSignatureBtn;
    private Button cancelSignatureBtn;
    private Bitmap signatureBitmap;
    private RelativeLayout signatureViewFrame;
    private SignatureView signatureView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature);

        useSignatureBtn = (Button) findViewById(R.id.btn_use_signature);
        clearSignatureBtn = (Button) findViewById(R.id.btn_clear_signature);
        cancelSignatureBtn = (Button) findViewById(R.id.btn_cancel_signature);

        useSignatureBtn.setText("Use");
        clearSignatureBtn.setText("Clear");
        cancelSignatureBtn.setText("Cancel");

        useSignatureBtn.setOnClickListener(this);
        clearSignatureBtn.setOnClickListener(this);
        cancelSignatureBtn.setOnClickListener(this);

        signatureViewFrame = (RelativeLayout) findViewById(R.id.frame_signature_canvas);
        signatureView = new SignatureView(this, null);
        signatureView.setBackgroundColor(Color.WHITE);
        signatureViewFrame.addView(signatureView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_use_signature) {
            Bitmap signatureBitmap =  Bitmap.createBitmap(signatureView.getWidth(), signatureView.getHeight(), Bitmap.Config.RGB_565);;

            Canvas canvas = new Canvas(signatureBitmap);
            signatureView.draw(canvas);

            Intent resultIntent = new Intent();
            if(signatureBitmap != null) {

                File photoCacheDir = FormActivity.getDirectory(FormActivity.DIRECTORY_NAME_CAMERA_CACHE);

                File file = new File(photoCacheDir, String.format("SIGN_%d.jpg", System.currentTimeMillis()));

                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(file);
                    signatureBitmap.compress(Bitmap.CompressFormat.JPEG, 0, fos);
                    fos.flush();
                    fos.close();
                    String targetFilename = file.getAbsolutePath();

                    Toast.makeText(this, "File name = " + targetFilename, Toast.LENGTH_SHORT).show();

                    resultIntent.putExtra(FormActivity.KEY_NEW_SIGNATURE_FILENAME, targetFilename);
                    setResult(Activity.RESULT_OK, resultIntent);
                    finish();
                } catch (FileNotFoundException e) {
                    setResult(Activity.RESULT_CANCELED, resultIntent);
                    finish();
                } catch (IOException e) {
                    setResult(Activity.RESULT_CANCELED, resultIntent);
                    finish();
                }
            } else {
                setResult(Activity.RESULT_CANCELED, resultIntent);
                finish();
            }

            setResult(Activity.RESULT_CANCELED, resultIntent);
            finish();
        } else if(v.getId() == R.id.btn_clear_signature) {
            signatureBitmap = null;
            signatureView.clear();
        } else if(v.getId() == R.id.btn_cancel_signature) {
            signatureBitmap = null;
            finish();
        }
    }

    public class SignatureView extends View {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public SignatureView(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }


        public void clear() {
            path.reset();
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:

                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;

                default:
                    debug("Ignored touch event: " + event.toString());
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;


            return true;
        }

        private void debug(String string) {
        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }
}
