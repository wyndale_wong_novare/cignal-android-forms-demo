package hk.com.novare.CignalAndroidFormDemo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.File;
import java.util.*;

/**
 * Manager class that handles the queuing, asynchronous loading and caching of photos.
 * @author Wyndale Wong
 */
public class AsyncPhotoLoadingManager {

	/** Reference to the singleton instance of the photo loading manager. */
	private static AsyncPhotoLoadingManager mInstance;

	/** Reference to member photo cache map. */
	private Map<String, Bitmap> photoCache;
	
	/** Reference to member photo container cache file. */
	private Map<ImageView, String> containerCache;
	
	/** Reference to member photo queue stack. */
	private final Stack<PhotoToLoad> photosToLoad;

	/** Reference to member photo loading worker thread. */
	private final PhotoLoaderThread 		photoLoaderThread;

	public static final String 	NULL = 								"null";
	
	/** Defined index for attachment type. */
	public static final int 				TYPE_ATTACHMENT = 			1;
	
	/** Defined maximum limit for soft memory cache. */
	private static final int 				CLEAR_SOFT_CACHE_LIMIT = 	500;
	
	/** Defined string for this class's log tag. */
	private static final String TAG = AsyncPhotoLoadingManager.class.getSimpleName();

	/**
	 * Constructor method for this class.
	 * @see PhotoLoaderThread
	 * @see PhotoToLoad
	 */
	public AsyncPhotoLoadingManager() {
		Log.d(TAG, "invoked constructor method");

		// initialize the soft memory cache for photo
		photoCache = 		Collections.synchronizedMap(new HashMap<String, Bitmap>());

		// initialize the soft memory cache for photo container references
		containerCache = 	Collections.synchronizedMap(new HashMap<ImageView, String>());

		// initialize the worker thread
		photoLoaderThread = new PhotoLoaderThread();

		// initialize the photo queue stack
		photosToLoad = 		new Stack<PhotoToLoad>();
	}

	/**
	 * Retrieves the singleton instance of the photo loading manager.
	 * @return The manager instance
	 * @see AsyncPhotoLoadingManager
	 */
	public static AsyncPhotoLoadingManager getInstance() {
		if(mInstance == null) {
			mInstance = new AsyncPhotoLoadingManager();
		}
		return mInstance;
	}

	/**
	 * Shuts down the worker thread and clear the cache.
	 */
	public void shutdown() {
		Log.d(TAG, "invoked stopUtils");
		try {
			clearCache();
			if (photoLoaderThread != null) {
				photoLoaderThread.interrupt();
			}
		} catch (final Exception e) {
			Log.d(TAG, "Exception: " + e.getMessage(), e);
		}
	}

	/**
	 * Binds the photo from the cache to the image container, and queues the loading task if not available.
	 * @param imageView The image container
	 * @param key The photo key
	 * @param type The type of photo to load
	 * @see PhotoUtil#getPhotoFromFile(java.io.File, boolean)
	 */
	public void displayImage(final ImageView imageView, final String key, final int type) {
		// turn visibility of the image container
		imageView.setVisibility(View.VISIBLE);

		// validate the photo key
		String newKey = NULL;
		if (key != null) {
			newKey = key;
		} else if ("".equals(key)) {
			newKey = NULL;
		}

		// place image container reference to cache
		containerCache.put(imageView, newKey);

		if (newKey != null && !NULL.equals(newKey)) {
			if (photoCache.containsKey(newKey)) {
				imageView.clearAnimation();

				// binds the photo from cache if available
				imageView.setImageBitmap(photoCache.get(newKey));
			} else {
				imageView.setImageResource(R.drawable.default_file);
				// queue the photo loading task if not found in both file cache and soft memory cache
				queuePhoto(newKey, type);
			}
		} else {
			imageView.setImageResource(R.drawable.default_file);
		}
	}

	/**
	 * Queues the photo for asynchronous loading.
	 * @param key The photo key
	 * @param type The type of photo to load
	 * @see #removeIfInStack(String)
	 */
	private void queuePhoto(final String key, final int type) {
		if (photoLoaderThread.getState() == Thread.State.NEW) {
			photoLoaderThread.start();
		}

		synchronized (photosToLoad) {
			removeIfInStack(key);
			final PhotoToLoad p = new PhotoToLoad(key, type);
			photosToLoad.push(p);
			photosToLoad.notifyAll();
		}
	}

	/**
	 * Class that defines the PhotoToLoad Java object.
	 * @author Wyndale Wong
	 */
	private class PhotoToLoad {
		private String key;
		private int photoType;

		public PhotoToLoad(final String newKey, final int type) {
			key = newKey;
			photoType = type;
		}

		public String getKey() {
			return key;
		}

		public int getPhotoType() {
			return photoType;
		}
	}

	/**
	 * Removes the existing photo loading task in the queue with the new one.
	 * @param key The photo key
	 */
	private void removeIfInStack(final String key) {
		Log.d(TAG, "invoked replaceIfInStack");
		int j = 0;
		while(j < photosToLoad.size()) {
			if (photosToLoad.get(j).getKey() == key) {
				photosToLoad.remove(j);
				Log.d(TAG, "at replaceIfInStack, removed at j = " + j);
			} else {
				j++;
			}
		}
	}

	/**
	 * Clears the soft memory cache and photo file cache based on the clean up policies.
	 */
	private void clearCache() {

		// clear the soft memory cache
		photoCache.clear();
		containerCache.clear();

		// clear photo loading task queue
		photosToLoad.clear();
	}

	/**
	 * Binds the photo to the image container in a task that runs on UI thread.
	 * @param key The photo key
	 * @param containerCache The soft memory cache of the image container references
	 * @param bitmap The actual photo to bind
	 * @see #getImageViews(java.util.Map, String)
	 * @see PhotoBinder
	 */
	private void bindPhoto(final String key, final Map<ImageView, String> containerCache,
			final Bitmap bitmap) {
		
		boolean keyIsNotNull = key != null && !"".equals(key) && !"null".equals(key);
		if (keyIsNotNull && bitmap != null && containerCache.containsValue(key)) {
			Log.d(TAG, "at bindPhoto, bitmap found in cache, key = " + key);
			// retrieve the list of image containers after validating the photo and the key
			final List<ImageView> containers = getImageViews(containerCache, key);

			for (final ImageView container : containers) {
				if (container != null) {
					// create a runnable per container to bind
					final PhotoBinder photoBinder = new PhotoBinder(bitmap, container);
					final Context context = container.getContext();
					Log.d(TAG, "at attachPhoto, container = " + container.getId());
					if (context instanceof Activity) {
						// run the photo binding task on UI thread
						((Activity) context).runOnUiThread(photoBinder);
					} else {
						Log.d(TAG, "c is not Activity");
					}
				}
			}
		} 
	}

	/**
	 * Retrieves the list of image container references given the photo key.
	 * @param containerCache The soft memory cache of the image container references
	 * @param key The photo key
	 * @return The List object
	 */
	private List<ImageView> getImageViews(final Map<ImageView, String> containerCache,
			final String key) {
		final List<ImageView> containers = new ArrayList<ImageView>();

		for (final ImageView container : containerCache.keySet()) {
			if (containerCache.get(container).equals(key)) {
				containers.add(container);
			}
		}

		return containers;
	}
	
	/**
	 * Thread class that defines the worker thread of the photo loading manager.
	 * @author Wyndale Wong
	 */
	private class PhotoLoaderThread extends Thread {
		public void run() {
			try {
				while (true) {	// infinite loop
					if (photosToLoad.isEmpty()) {
						synchronized (photosToLoad) {
							// thread waits until the semaphore to release its lock if queue is empty
							photosToLoad.wait();
						}
					}
					if (!photosToLoad.isEmpty()) {
						PhotoToLoad photoToLoad;
						synchronized (photosToLoad) {
							// retrieves the last task item of the photo loading queue
							photoToLoad = photosToLoad.pop();
						}
						
						Bitmap bmp = null;
						if(photoToLoad.getPhotoType() == TYPE_ATTACHMENT) {
							File file = new File(photoToLoad.getKey());
							// decode the photo from the actual source
							bmp = PhotoUtil.getPhotoFromFile(file, true);
						}
						
						
						synchronized (photoCache) {
							if (photoCache.size() > CLEAR_SOFT_CACHE_LIMIT) {
								photoCache.clear();
							}
							if (bmp != null) {
								// save the photo to the soft memory cache after decoding
								photoCache.put(photoToLoad.getKey(), bmp);
							}
						}
						
						synchronized(containerCache) {
							// binds the photo to the image container
							bindPhoto(photoToLoad.getKey(), containerCache, bmp);
						}
					}
					if (Thread.interrupted()) {
						// break from the loop if the thread is interrupted
						break;
					}
				}
			} catch (final InterruptedException e) {
				Log.d(TAG, "InterruptedException: " + e.getMessage(), e);
			}
		}
	}

	

	/**
	 * Class that implements the runnable task for binding photos to the image container.
	 * @author Wyndale Wong
	 */
	private class PhotoBinder implements Runnable {
		
		/** Photo to bind. */
		private Bitmap bitmap;
		
		/** Image container. */
		private ImageView imageView;

		/**
		 * Constructor method for this class.
		 * @param bitmap The photo to bind.
		 * @param imageView The image container
		 */
		public PhotoBinder(final Bitmap bitmap, final ImageView imageView) {
			this.bitmap = bitmap;
			this.imageView = imageView;
		}

		/**
		 * Defines the photo binding task logic.
		 */
		public void run() {
			if (bitmap != null) {
				imageView.setVisibility(View.VISIBLE);
				imageView.clearAnimation();
				imageView.setBackgroundResource(0);
				imageView.setImageBitmap(bitmap);
			} else {
				imageView.setImageResource(R.drawable.default_file);
			}
		}
	}
}
