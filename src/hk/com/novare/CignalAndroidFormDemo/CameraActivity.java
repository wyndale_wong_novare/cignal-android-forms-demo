package hk.com.novare.CignalAndroidFormDemo;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;


public class CameraActivity extends Activity implements SurfaceHolder.Callback, View.OnClickListener {

    private final String TAG = CameraActivity.class.getName();
    private Camera mCamera;
    private SurfaceView mSurfaceView;
    private SurfaceHolder mSurfaceHolder;
    private Bitmap photo;

    private Camera.ShutterCallback shutterCallback;
    private Camera.PictureCallback jpegCallback;

    private ImageView ivPhotoPreview;
    private Button btnCapturePhoto;
    private Button btnUsePhoto;
    private Button btnTakeNewPhoto;
    private Button btnCancelPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        if (Build.VERSION.SDK_INT >= 23) {
            requestPermission();
        } else {
            init();
        }
    }

    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            init();
        }
    }

    private void init() {
        mSurfaceView = (SurfaceView) findViewById(R.id.sv_camera_preview);
        mSurfaceHolder = mSurfaceView.getHolder();

        ivPhotoPreview = (ImageView) findViewById(R.id.iv_photo_preview);
        btnCapturePhoto = (Button) findViewById(R.id.btn_capture_photo);
        btnUsePhoto = (Button) findViewById(R.id.btn_use_photo);
        btnTakeNewPhoto = (Button) findViewById(R.id.btn_take_new_photo);
        btnCancelPhoto = (Button) findViewById(R.id.btn_cancel_photo);
        ivPhotoPreview.setVisibility(View.GONE);
        btnUsePhoto.setText("Use");
        btnTakeNewPhoto.setText("Retry");
        btnCancelPhoto.setText("Cancel");
        btnCapturePhoto.setText("Capture");
        btnUsePhoto.setVisibility(View.GONE);
        btnTakeNewPhoto.setVisibility(View.GONE);

        btnCapturePhoto.setOnClickListener(this);
        btnUsePhoto.setOnClickListener(this);
        btnTakeNewPhoto.setOnClickListener(this);
        btnCancelPhoto.setOnClickListener(this);

        photo = null;

        mSurfaceHolder.addCallback(this);
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        jpegCallback = new Camera.PictureCallback() {

            @Override
            public void onPictureTaken(byte[] data, Camera camera) {

                Bitmap tempPhoto = BitmapFactory.decodeByteArray(data, 0, data.length);

                Matrix matrix = new Matrix();

                matrix.postRotate(90);

                photo = Bitmap.createBitmap(tempPhoto, 0, 0, tempPhoto.getWidth(),
                        tempPhoto.getHeight(), matrix, true);

                if(photo != null) {
                    ivPhotoPreview.setImageBitmap(photo);
                    ivPhotoPreview.setVisibility(View.VISIBLE);
                    mSurfaceView.setVisibility(View.GONE);
                    btnCapturePhoto.setVisibility(View.GONE);
                    btnUsePhoto.setVisibility(View.VISIBLE);
                    btnTakeNewPhoto.setVisibility(View.VISIBLE);
                }

                refreshCamera();
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    init();
                } else {
                    finish();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void captureImage(View v) throws IOException {
        mCamera.takePicture(null, null, jpegCallback);
    }

    public void refreshCamera() {
        if (mSurfaceHolder.getSurface() == null) {
            return;
        }

        try {
            mCamera.stopPreview();
        }

        catch (Exception e) {
        }

        try {
            mCamera.setPreviewDisplay(mSurfaceHolder);
            mCamera.startPreview();
        }
        catch (Exception e) {
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        photo = null;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
        }

        catch (RuntimeException e) {
            System.err.println(e);
            return;
        }

        Camera.Parameters param;
        param = mCamera.getParameters();

        Camera.Size bestSize = null;
        List<Camera.Size> sizeList = mCamera.getParameters().getSupportedPreviewSizes();
        bestSize = sizeList.get(0);
        for(int i = 1; i < sizeList.size(); i++){
            if((sizeList.get(i).width * sizeList.get(i).height) > (bestSize.width * bestSize.height)){
                bestSize = sizeList.get(i);
            }
        }

        List<Integer> supportedPreviewFormats = param.getSupportedPreviewFormats();
        Iterator<Integer> supportedPreviewFormatsIterator = supportedPreviewFormats.iterator();
        while(supportedPreviewFormatsIterator.hasNext()){
            Integer previewFormat =supportedPreviewFormatsIterator.next();
            if (previewFormat == ImageFormat.YV12) {
                param.setPreviewFormat(previewFormat);
            }
        }

        param.setPreviewSize(bestSize.width, bestSize.height);
        param.setPictureSize(bestSize.width, bestSize.height);
        param.setJpegQuality(100);
        mCamera.setParameters(param);

        try {
            Camera.Parameters parameters = mCamera.getParameters();
            parameters.setRotation(90); //set rotation to save the picture
            mCamera.setDisplayOrientation(90);
            mCamera.setPreviewDisplay(mSurfaceHolder);
            mCamera.startPreview();
        }

        catch (Exception e) {
            System.err.println(e);
            return;
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        refreshCamera();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if(mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_capture_photo) {
            try {
                captureImage(mSurfaceView);
            } catch(Exception e) {
                Log.d(TAG, "Error capture photo");
            }
        } else if(v.getId() == R.id.btn_use_photo) {

            Intent resultIntent = new Intent();
            if(photo != null) {
                File photoCacheDir = FormActivity.getDirectory(FormActivity.DIRECTORY_NAME_CAMERA_CACHE);

                File file = new File(photoCacheDir, String.format("PHOTO_%d.jpg", System.currentTimeMillis()));

                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(file);
                    photo.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                    fos.flush();
                    fos.close();
                    String targetFilename = file.getAbsolutePath();

                    Toast.makeText(this, "File name = " + targetFilename, Toast.LENGTH_SHORT).show();

                    resultIntent.putExtra(FormActivity.KEY_NEW_PHOTO_FILENAME, targetFilename);
                    setResult(Activity.RESULT_OK, resultIntent);
                    photo = null;
                    finish();
                } catch (FileNotFoundException e) {
                    setResult(Activity.RESULT_CANCELED, resultIntent);
                    photo = null;
                    finish();
                } catch (IOException e) {
                    setResult(Activity.RESULT_CANCELED, resultIntent);
                    photo = null;
                    finish();
                }
            } else {
                setResult(Activity.RESULT_CANCELED, resultIntent);
                photo = null;
                finish();
            }
        } else if(v.getId() == R.id.btn_take_new_photo) {
            photo = null;
            ivPhotoPreview.setVisibility(View.GONE);
            mSurfaceView.setVisibility(View.VISIBLE);
            btnTakeNewPhoto.setVisibility(View.GONE);
            btnUsePhoto.setVisibility(View.GONE);
            btnCapturePhoto.setVisibility(View.VISIBLE);
        } else if(v.getId() == R.id.btn_cancel_photo) {
            photo = null;
            finish();
        }
    }


}
