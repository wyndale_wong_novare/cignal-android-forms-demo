package hk.com.novare.CignalAndroidFormDemo;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FormActivity extends Activity implements View.OnClickListener{

    private final String TAG = FormActivity.class.getName();

    public static final String DIRECTORY_NAME_CAMERA_CACHE = 		"CignalPhotoCache";

    public static final int CAPTURE_PHOTO_REQUEST = 1;

    public static final int CAPTURE_SIGNATURE_REQUEST = 2;

    public static final String KEY_NEW_SIGNATURE_FILENAME = "signature";

    public static final String KEY_NEW_PHOTO_FILENAME = "photo";

    private ImageView signaturePreviewIV;
    private LinearLayout photoPreviewFrame;
    private Button addPhotoBtn;
    private Button setSignatureBtn;
    private Button removeSignatureBtn;

    private List<String> photoFilenames;
    private String signatureFilename;
    private TextView signatureFilenameTV;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        photoFilenames = new ArrayList<String>();
        signatureFilename = null;

        addPhotoBtn = (Button) findViewById(R.id.btn_add_photo);
        setSignatureBtn = (Button) findViewById(R.id.btn_set_signature);
        removeSignatureBtn = (Button) findViewById(R.id.btn_signature_remove);

        addPhotoBtn.setText("Add Photo");
        setSignatureBtn.setText("Set Signature");

        addPhotoBtn.setOnClickListener(this);
        setSignatureBtn.setOnClickListener(this);
        removeSignatureBtn.setOnClickListener(this);

        signatureFilenameTV = (TextView) findViewById(R.id.tv_signature_file_name);
        signaturePreviewIV = (ImageView) findViewById(R.id.iv_signature_thumb);
        photoPreviewFrame = (LinearLayout) findViewById(R.id.frame_photo_preview);

        photoPreviewFrame.setVisibility(View.GONE);
        signaturePreviewIV.setVisibility(View.GONE);
        removeSignatureBtn.setVisibility(View.GONE);
    }

    public static File getDirectory(String directoryName) {
        File cacheDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                directoryName);
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        return cacheDir;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == CAPTURE_PHOTO_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                String newPhotoFilename = data.getStringExtra(KEY_NEW_PHOTO_FILENAME);
                if(photoFilenames != null) {
                    photoFilenames.add(newPhotoFilename);
                } else {
                    photoFilenames = new ArrayList<String>();
                }
                refreshPhotos();
            }
        } else if (requestCode == CAPTURE_SIGNATURE_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                String tempFilename = data.getStringExtra(KEY_NEW_SIGNATURE_FILENAME);
                if(tempFilename != null) {
                    signaturePreviewIV.setImageBitmap(null);
                    if(signatureFilename != null) {
                        (new File(signatureFilename)).delete();
                    }
                    signatureFilename = tempFilename;
                    refreshSignature();
                }
            }
        }
    }

    private void refreshPhotos() {
        photoPreviewFrame.removeAllViews();
        if(photoFilenames != null && photoFilenames.size() > 0) {
            for(int position = 0; position < photoFilenames.size(); position++) {
                View photoThumbView = getPhotoThumbView(photoFilenames.get(position), position);
                photoPreviewFrame.addView(photoThumbView);
            }
            photoPreviewFrame.setVisibility(View.VISIBLE);
        } else {
            photoPreviewFrame.setVisibility(View.GONE);
        }
    }

    private void refreshSignature() {
        if(signatureFilename != null) {
            File file = new File(signatureFilename);
            Bitmap signature = PhotoUtil.getPhotoFromFile(file, false);
            signaturePreviewIV.setImageBitmap(signature);
            signaturePreviewIV.setBackgroundColor(Color.WHITE);
            signaturePreviewIV.setVisibility(View.VISIBLE);
            signatureFilenameTV.setText(file.getName());
            signatureFilenameTV.setVisibility(View.VISIBLE);
            removeSignatureBtn.setVisibility(View.VISIBLE);
        } else {
            signatureFilenameTV.setVisibility(View.GONE);
            signaturePreviewIV.setVisibility(View.GONE);
            removeSignatureBtn.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_add_photo) {
            Intent photoIntent = new Intent(this, CameraActivity.class);
            startActivityForResult(photoIntent, CAPTURE_PHOTO_REQUEST);
        } else if(view.getId() == R.id.btn_set_signature) {
            Intent signatureIntent = new Intent(this, SignatureActivity.class);
            startActivityForResult(signatureIntent, CAPTURE_SIGNATURE_REQUEST);
        } else if(view.getId() == R.id.btn_signature_remove) {
            signaturePreviewIV.setImageBitmap(null);
            (new File(signatureFilename)).delete();
            signaturePreviewIV.setVisibility(View.GONE);
            removeSignatureBtn.setVisibility(View.GONE);
            signatureFilenameTV.setVisibility(View.GONE);
        }
    }

    public View.OnClickListener getRemoveListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(photoFilenames != null && photoFilenames.size() > position) {
                    String filename = photoFilenames.get(position);
                    (new File(filename)).delete();
                    photoFilenames.remove(position);
                    refreshPhotos();
                }
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        File photoCacheDir = FormActivity.getDirectory(FormActivity.DIRECTORY_NAME_CAMERA_CACHE);
        String[] children = photoCacheDir.list();
        Log.d(TAG, "Directory file count = " + children.length);
        for (int i = 0; i < children.length; i++) {
            String filename = children[i];
            Log.d(TAG, "File found = " + filename);
            if (filename.endsWith(".jpeg") || filename.endsWith(".jpg"))
                (new File(photoCacheDir, filename)).delete();
        }
    }

    private View getPhotoThumbView(String filename, int position) {
        View view = getLayoutInflater().inflate(R.layout.list_item_photos, null);
        // initialize views
        ImageView thumbIV = 	(ImageView) view.findViewById(R.id.iv_photo_thumb);
        TextView filenameTV = 	(TextView) 	view.findViewById(R.id.tv_photo_file_name);
        Button removeBtn = 		(Button) 	view.findViewById(R.id.btn_photo_remove);

        // instantiate file object
        File file = new File(filename);

        // check if file exists
        if(file.exists()) {
            filenameTV.setText(file.getName());

            // check if file is a photo
            Bitmap photo = PhotoUtil.getPhotoFromFile(file, false);
            thumbIV.setImageBitmap(photo);
        } else {
            filenameTV.setText("File not found");
            thumbIV.setImageResource(R.drawable.default_file);
        }
        removeBtn.setOnClickListener(getRemoveListener(position));

        return view;
    }
}
